_clibroot = "classes/cLib/classes/"
_vlibroot = "classes/vLib/classes/"
require (_clibroot.."cLib")
require (_vlibroot..'vLib')
require (_vlibroot.."parsers/vXML")
require (_vlibroot..'vTree')
require (_vlibroot.."helpers/vSelection")
local inspect = require 'inspect'

local DIALOG_MARGIN = renoise.ViewBuilder.DEFAULT_DIALOG_MARGIN
local CONTENT_SPACING = renoise.ViewBuilder.DEFAULT_CONTROL_SPACING

_AUTO_RELOAD_DEBUG = function()
  show_palette()
end

local function show_status(message)
  renoise.app():show_status(message)
  print(message)
end

local function toggle_palette_display()
end

function generate_instrument_data()
  local placeholder = {}
  local instruments = renoise.song().instruments
  local total = #instruments

  for i = 1,total do
    local instrument = instruments[i]
    local instrument_name = instrument.name
    table.insert(placeholder, {name = instrument_name})
  end

  return placeholder
end

function generate_library_data()
  local library_data = {
    {
      name = "First",
    },
    {
      name = "Second",
    },
    {
      name = "Node",
      expanded = true,
      {
        name = "Item #1",
      },
      {
        name = "Item #2",
      }
    },
    {
      name = "Node with toggle event",
      expanded = false,
      {
        name = "Item #1",
      },
      {
        name = "Item #2",
      },
    },
    {
      name = "Node",
      {
        name = "Item #1",
      },
      {
        name = "Item #2",
      },
      {
        name = "Item #3",
      },
      {
        name = "Item #4",
      },
      {
        name = "Item #5",
      },
      {
        name = "Item #6",
      },
      {
        name = "Item #7",
      },
      {
        name = "Item #8",
      },
      {
        name = "Item #9",
      },
    },
  }
  return library_data
end


function show_palette(state)
  local palette_dialog = nil
  local vb = renoise.ViewBuilder()

  --local TEXT_ROW_WIDTH = 80

  local dialog_content = vb:column {
    margin = DIALOG_MARGIN,

    vb:row {
      vb:column { id = "library_panel", spacing = CONTENT_SPACING },
      vb:column { id = "separator", width = 35 },
      vb:column { id = "instrument_panel" },
    },
  }

  local library_vtree = vTree {
    id = "library_vtree",
    vb = vb,
    width = 340,
    height = 150,
    num_rows = 15,
    data = generate_library_data(),
    autosize = true,
    --require_selection = true,
    on_select = function(self, item)
      self:set_selected_index(item.item_id)
      show_status(('Changed library instrument to %s'): format(item.name))
    end,
  }

  local row_filter = vb:row {
    vb:textfield {
      width = 120,
      edit_mode = true,
      text = "Filter...",
      notifier = function(text)
        show_status(("textfield value changed to '%s'"):
          format(text))
      end
    }
  }

  local instrument_vtree = vTree {
    id = "instrument_vtree",
    vb = vb,
    width = 340,
    num_rows = 15,
    data = generate_instrument_data(),
    autosize = false,
    on_select = function(self, item)
      --renoise.song().selected_instrument.name
      self:set_selected_index(item.item_id)
      renoise.song().selected_instrument_index = item.item_id - 1000
      show_status(("Instrument changed to '%s'"):
        format(item.name))
    end
  }

  local row_instrument_controls = vb:row {
    -- spacing between two main columns
    vb:text {
      width = 30,
    },
    vb:button {
      text = "<",
      notifier = function()
        local idx = instrument_vtree:get_selected_index()
        instrument_vtree:set_selected_index(idx - 1)

        renoise.song().selected_instrument_index = idx - 1000 - 1
        --show_status("prev instrument button was hit")
      end,
    },
    vb:button {
      -- buttons can also have custom text/back colors
      text = "x",
      color = {0xaa, 0x00, 0x00},
      -- and we also can handle presses, releases separately
      notifier = function()
        -- TODO
        --local idx = instrument_vtree:get_selected_index()
        --print(idx)
        --local instrument_name = instrument_vtree:get_row_by_id(idx)
        --print(instrument_name)
        --show_status(("%s deleted"): format(instrument_name))
      end,
    },
    vb:button {
      text = ">",
      notifier = function()
        local idx = instrument_vtree:get_selected_index()
        instrument_vtree:set_selected_index(idx + 1)
        renoise.song().selected_instrument_index = idx - 1000 + 1
        --show_status("next instrument button was hit")
      end,
    }
  }

  vb.views['library_panel']:add_child(library_vtree.view)
  vb.views['library_panel']:add_child(row_filter)


  vb.views['instrument_panel']:add_child(instrument_vtree.view)
  vb.views['instrument_panel']:add_child(row_instrument_controls)

  palette_dialog = renoise.app():show_custom_dialog(
    "Palette", dialog_content
  )

end

-----------------------------------------------------------------------------

function handle_key_events()

  -- dialogs also allow you to handle keyboard events by your own. by default
  -- only the escape key is used to close the dialog when focused. If you want
  -- to do more fancy stuff, then simply pass a key_hander_func to the custom
  -- dialog. Here is a simply example how this can be done:

  local vb = renoise.ViewBuilder()

  local DIALOG_MARGIN = renoise.ViewBuilder.DEFAULT_DIALOG_MARGIN
  local CONTENT_SPACING = renoise.ViewBuilder.DEFAULT_CONTROL_SPACING

  local TEXT_ROW_WIDTH = 240

  local content_view =  vb:column {
    margin = DIALOG_MARGIN,
    spacing = CONTENT_SPACING,

    vb:text {
      width = TEXT_ROW_WIDTH,
      text = "Press some keyboard keys:"
    },

    vb:row {
      style = "group",

      vb:multiline_text {
        id = "key_text",
        width = TEXT_ROW_WIDTH,
        height = 92,
        paragraphs = {
          "key.state:",
          "key.name:",
          "key.modifiers:",
          "key.character:",
          "key.note:",
          "key.repeat:",
        },
        font = "mono",
      }
    }
  }

  local function key_handler(dialog, key)

    -- update key_text to show what we got
    vb.views.key_text.paragraphs = {
      ("key.state: '%s'"):format(key.state or "nil"),
      ("key.name: '%s'"):format(key.name),
      ("key.modifiers: '%s'"):format(key.modifiers),
      ("key.character: '%s'"):format(key.character or "nil"),
      ("key.note: '%s'"):format(tostring(key.note) or "nil"),
      ("key.repeated: '%s'"):format(key.repeated and "true" or "false")
    }

    -- close on escape...
    if (key.modifiers == "" and key.name == "esc") then
      dialog:close()
    end
  end

  -- handler options
  local options = {
    send_key_repeat = true,
    send_key_release = true
  }
  -- show a dialog as usual, but this time also pass a keyboard handler ref
  renoise.app():show_custom_dialog(
    "Handling Keyboard Events", content_view, key_handler, options)

end

--------------------------------------------------------------------------------

-- documents_and_views

-- as already noted in 'available_controls'. views can also be attached to
-- external document values, in order to seperate the controller code from the
-- view code. We're going to do this tight now and do start by create a very
-- simple example document. Please have a look at Renoise.Document.API for more
-- detail about such documents


-- DOCUMENT

-- create a simple document
--[[
local example_document = renoise.Document.create("ExampleDocument") {
  my_flag = false,
  some_velocity = 127,
  pad_x = 0.5,
  pad_y = 0.5
}

-- notifier callbacks
local function my_flag_notifier()
  local my_flag_value = example_document.my_flag.value

  show_status(("'my_flag' changed to '%s' by either the GUI "..
    "or something else..."):format(my_flag_value and "True" or "False"))
end

local function some_velocity_notifier()
  local some_velocity = example_document.some_velocity.value

  show_status(("'some_velocity' value changed to '%s' by either the GUI "..
    "or something else..."):format(some_velocity))
end

local function pad_value_notifier()
  local x, y = example_document.pad_x.value, example_document.pad_y.value

  show_status(("'pad_xy' value changed to '%.2f,%.2f' by either the GUI "..
    "or something else..."):format(x, y))
end

-- attach to the document
example_document.my_flag:add_notifier(my_flag_notifier)
example_document.some_velocity:add_notifier(some_velocity_notifier)

example_document.pad_x:add_notifier(pad_value_notifier)
example_document.pad_y:add_notifier(pad_value_notifier)
--]]


-- Menu entries
renoise.tool():add_menu_entry {
  name = "Main Menu:Tools:Example Tool GUI:7. Available Controls...",
  invoke = function() available_controls() end
}

renoise.tool():add_menu_entry {
  name = "Main Menu:Tools:Example Tool GUI:9. Keyboard Events...",
  invoke = function() handle_key_events() end
}

renoise.tool():add_menu_entry {
  name = "Main Menu:Tools:Palette",
  invoke = function() show_palette() end
}

-- Key bindings
renoise.tool():add_keybinding {
  name = "Global:Instruments:Show Palette (tool)",
  invoke = show_palette
}

